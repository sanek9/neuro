#include "neuro.h"
/*
neur_t activ_f(neur_t x){
    return 1 / (1+exp(-x));
}
neur_t pr_f(neur_t x){
    return x * (1-x);
}*/
neur_t activ_f(neur_t x){
    return 2. / (1+exp(-0.5*x)) - 1;
}
neur_t pr_f(neur_t x){
    return 0.5 * (1 + x) * (1-x);
}

int save_net(Neuronet *net, char *path){
    FILE *f;
    f = fopen(path, "w");
    if(!f)
        return -1;
    uint32_t num = net->layers;

    fwrite(fileid, sizeof(char), strlen(fileid), f);
    fwrite(&num, sizeof(num), 1, f);
    fwrite(net->layn, sizeof(neur_t), num+1, f);
    size_t i, j;
    for(i = 0; i < num; i++){
        for(j = 0; j < net->layn[i]+1; j++){
            fwrite(net->net[i][j], sizeof(neur_t), net->layn[i+1], f);
        }
    }
    fclose(f);
    return 0;
}
int load_net(Neuronet *net, char *path){
    FILE *f;
    f = fopen(path, "r");
    if(!f)
        return -1;
    uint32_t num;
    char buf[32] = {0};
    fread(buf, sizeof(char), strlen(fileid), f);
    if(strcmp(fileid, buf)){
        fclose(f);
        return -1;
    }
    fread(&num, sizeof(num), 1, f);
    neur_t arr[num+1];

    fread(arr, sizeof(neur_t), num+1, f);
    init_neuronet(net, num, arr);
    size_t i, j;
    for(i = 0; i < num; i++){
        for(j = 0; j < net->layn[i]+1; j++){
            fread(net->net[i][j], sizeof(neur_t), net->layn[i+1], f);
        }
    }
    fclose(f);
}
void _calc_layer(neur_t *neuron, neur_t *input, neur_t **link, int inputs, int neurons ){

    int i, j;
    neur_t sum;
    for(i = 0; i < neurons; i++){
        sum = 0;
        for(j = 0; j < inputs; j++){
            sum += input[j] * link[j][i];
        }
        sum += link[j][i]; //bias
        neuron[i] = activ_f(sum);
    }
}
void _calc_delta(neur_t *delta, neur_t *idelta, neur_t **link, int ideltas, int deltas){
    int i, j;
    neur_t sum;
    for(i = 0; i < deltas; i++){
        sum = 0;
        for(j = 0; j < ideltas; j++){
            sum += idelta[j] * link[i][j];
        }
        delta[i] = sum;
    }
}

void _calc_weight(neur_t *neuron, neur_t *input, neur_t **link, neur_t *delta, int inputs, int neurons, neur_t eta ){
    int i, j;
    neur_t sum;
    for(i = 0; i < neurons; i++){
        for(j = 0; j < inputs; j++){
            link[j][i] = link[j][i] + eta * delta[i] * input[j] * pr_f(neuron[i]);
        }
        link[j][i] = link[j][i] + eta * delta[i];//bias
    }

}
int learn_net(Neuronet *net, Neurons *neur, Ndeltas *d, neur_t *input, neur_t * res){
    calc_net(net, neur, input);

    int i;
    for(i = 0; i < neur->layn[neur->layers-1]; i++){
        d->neur[neur->layers-1][i] = res[i] - neur->neur[neur->layers-1][i];
    }
    for(i = net->layers-1; i>0; i--){
        _calc_delta(d->neur[i-1],d->neur[i], net->net[i], d->layn[i], d->layn[i-1]);
        _calc_weight(neur->neur[i], neur->neur[i-1],net->net[i], d->neur[i], neur->layn[i-1], neur->layn[i], 0.5);
    }
    _calc_weight(neur->neur[0], input, net->net[0], d->neur[0], net->layn[0], net->layn[1], 0.5);

    return 0;
}

int calc_net(Neuronet *net, Neurons *neur, neur_t *input){
    if(net==NULL)
        return -2;
    if(neur->layn!=&(net->layn[1]))
        return -1;
    int i;
    _calc_layer(neur->neur[0], input, net->net[0], net->layn[0], net->layn[1]);
    for(i = 1; i < neur->layers; i++){
        _calc_layer(neur->neur[i], neur->neur[i-1], net->net[i], net->layn[i], net->layn[i+1]);

    }


    return 0;
}

int init_neuronet(Neuronet* net, int layers, int* layn){
    int i, j, k;
    net->net = (neur_t***) calloc(layers, sizeof(neur_t**));
    net->layn = (int*) calloc(layers, sizeof(int));
    net->layers = layers;
    for(i = 0; i < layers+1; i++){
        net->layn[i] = layn[i];
    }

    if(!net->net){

        return -1;
    }

    for(i = 0; i < layers; i++){
        net->net[i] = (neur_t**)calloc(net->layn[i]+1, sizeof(neur_t*));
        for(j = 0; j < net->layn[i]+1; j++){
            net->net[i][j] = (neur_t*) calloc(net->layn[i+1], sizeof(neur_t));
            for(k = 0; k < net->layn[i+1]; k++){
                net->net[i][j][k]= (rand()%100)/100.-0.5;
            }
        }
    }
    return 0;
}

int init_neurons(Neurons *neur, Neuronet *net){
    int i;

    neur->layers = net->layers;
    neur->layn = &(net->layn[1]);
    neur->neur = (neur_t**) calloc(neur->layers, sizeof(neur_t*));
    for(i = 0; i < neur->layers; i++){
        neur->neur[i] = (neur_t*)calloc(neur->layn[i], sizeof(neur_t));

    }
    return 0;
}
