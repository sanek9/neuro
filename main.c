#include <stdio.h>
#include <stdlib.h>
#include "neuro.h"

neur_t map[][9] = {
{
    0, 0, 0,
    0, 0, 0,
    0, 0, 0
},
{
    0, 1, 0,
    0, 1, 0,
    0, 1, 0

},
{
    1, 1, 1,
    0, 1, 0,
    0, 1, 0
},
{
    1, 1, 1,
    1, 0, 0,
    1, 1, 1
},
{
    1, 1, 1,
    1, 0, 1,
    1, 1, 1
},
{
    0, 1, 0,
    1, 0, 1,
    1, 0, 1
}
};
int main()
{



    Neuronet net;
    Neurons neur;
    Ndeltas d;
    int i, j, k;
    int n = 5;
    int m = 0;
    for(i = 0; i < 3; i++){
        printf("%d ", (n >>i)&0x1);
        m = (m)|(((n >>i)&0x1)<<i);
    }
    printf("\n%d\n", m);
    int l[] = {9, 5, 3};
   // init_neuronet(&net, 2, l);
    load_net(&net, "neuro");
   // printf("load net\n");
    printf("layers: %d\n", net.layers);
    for(i = 0; i < net.layers+1; i++){
        printf("% d: n: %d\n",i, net.layn[i]);
    }
    printf("\n");
    init_neurons(&neur, &net);
    init_neurons(&d, &net);

    neur_t res[3] = {0, 0, 0};
   // neur_t res[3] = {0.9};

  //  calc_net(&net, &neur, inp);
    for(i = 0; i < 1000; i++){
        for(j = 0; j < 6; j++){
            for(k = 0; k < 3; k++){
                    res[k] = (j >>k)&0x1;
            }
            learn_net(&net, &neur, &d, map[j], res);
        }

    }
while(1){
    scanf("%d", &n);
    if (n == 9)
        break;
    if(n == 7){

        for(i = 0; i < net.layers; i++){
            printf("\nlayer %d\n", i);
            for(j = 0; j < net.layn[i]+1; j++){
                for(k = 0; k < net.layn[i+1]; k++){
                    printf("% 0.3f ", net.net[i][j][k]);
                }
                printf("\n");


            }
        }
        printf("\n");
        continue;
    }
    if(n == 8){
        printf("study\n");
        scanf("%d", &n);
        for(i = 0; i < 3; i++){
            res[i] = (n >>i)&0x1;
        }
        learn_net(&net, &neur, &d, map[n], res);

    }
    for(i = 0; i < 9; i++){

        printf("%.3f ", map[n][i]);
        if((i+1)%3==0)
            printf("\n");
    }


    calc_net(&net, &neur, map[n]);
    printf("result\n");
    printf("\n");
    m = 0;
    for(i = 0; i < neur.layn[neur.layers-1]; i++){
        printf("%f ", neur.neur[neur.layers-1][i]);
        m = (m)|(((int)round(neur.neur[neur.layers-1][i]))<<i);
    }
    printf("\n%d\n", m);
    for(i = 0; i < 9; i++){

        printf("%.3f ", map[m][i]);
        if((i+1)%3==0)
            printf("\n");
    }

    printf("\n");
}
    save_net(&net, "neuro");
    printf("Hello world!\n");
    return 0;
}
