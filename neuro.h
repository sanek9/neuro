
#ifndef NEURO_H
#define NEURO_H
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>



typedef float neur_t;
static char fileid[] = "%NEUR-0.1";


typedef struct _Neuronet {

    int layers;
    int *layn;
    neur_t ***net;
}Neuronet;
typedef struct _Neurons{
    int layers;
    int *layn;
    neur_t ** neur;
}Neurons;
typedef Neurons Ndeltas;

int init_neuronet(Neuronet* net, int layers, int* layn);

int calc_net(Neuronet *net, Neurons *neur, neur_t *input);
int learn_net(Neuronet *net, Neurons *neur, Ndeltas *d, neur_t *input, neur_t * res);
int save_net(Neuronet *net, char *path);
int load_net(Neuronet *net, char *path);


//int destroy_neuronet(Neuronet* net);

#endif // NEURO_H
